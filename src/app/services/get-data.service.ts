import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment as env} from '../../environments/environment';
import { Data } from '../models/data';


@Injectable({
  providedIn: 'root'
})
export class GetDataService{

  constructor(private http: HttpClient) { }

  getNumberArray(): Observable<any> {
    const uri: string = env.api.getNumberURI;
    return this.http.get(uri);
  }

  getParagraphs(): Observable<any> {
    const uri: string = env.api.getParagraphURI;
    return this.http.get(uri);
  }
}
