import { Injectable } from '@angular/core';
import { Data } from '../models/data';

@Injectable({
  providedIn: 'root'
})
export class TransferDataService {
  data: Data;
  constructor() { }
}
