import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { Router } from '@angular/router';

import { GetDataService } from '../../services/get-data.service'
import { TransferDataService } from '../../services/transfer-data.service'

import { Data } from 'src/app/models/data';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {

  constructor(
    private router : Router,
    private dataSource : GetDataService,
    private transferDataService : TransferDataService
  ) { }

  isLoading: boolean= false;

  ngOnInit(): void {  }

  showNumberDetail(){
    this.isLoading = true;

    this.dataSource.getNumberArray().subscribe( numbers => {
      this.transferDataService.data = numbers;
      console.log(numbers);
      this.router.navigate(['/numberDetail']);
    },
    err => {
      console.log(err);
      this.router.navigate(['/error']);
    });

    this.isLoading = false;
  }

  showParagraphDetail(){
    this.isLoading = true;

    this.dataSource.getParagraphs().subscribe( paragraphData => {
      this.transferDataService.data = paragraphData;
      console.log(paragraphData);
      this.router.navigate(['/paragraphDetail']);
    },
    err => {
      console.log(err);
      this.router.navigate(['/error']);
    });

    this.isLoading = false;
  }

}
