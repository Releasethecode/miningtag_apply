import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { NumberDetailComponent } from './components/number-detail/number-detail.component';
import { ParagraphDetailComponent } from './components/paragraph-detail/paragraph-detail.component';
import { AppComponent } from './app.component';


const routes: Routes = [
  { path: 'numberDetail', component: NumberDetailComponent },
  { path: 'paragraphDetail', component: ParagraphDetailComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
