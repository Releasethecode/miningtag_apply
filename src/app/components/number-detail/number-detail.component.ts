import { Component, OnInit } from '@angular/core';
import { TransferDataService } from '../../services/transfer-data.service'
import { Data } from 'src/app/models/data';

@Component({
  selector: 'app-number-detail',
  templateUrl: './number-detail.component.html',
  styleUrls: ['./number-detail.component.css']
})
export class NumberDetailComponent implements OnInit {
  

  numberData: Data ;
  
  processedData: boolean = false;

  constructor(
    private transferDataService: TransferDataService
  ) { }

  ngOnInit(): void {
    this.numberData = this.transferDataService.data;
    this.transferDataService.data = undefined;
    console.log(this.numberData);
    this.processedData = true;
  }



}
